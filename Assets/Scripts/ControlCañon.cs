using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ControlCañon : MonoBehaviour
{
    public GameObject bala;
    public int cantidadBalas;
    public float tiempoCañon, velocidad;
    [SerializeField] public Direccion direccionSeleccionada;
    public enum Direccion { right, left, up, down, forward, back}

    private Queue<GameObject> balas;
    private float tiempo;

    void Start()
    {
        balas = new Queue<GameObject>();
        tiempo = tiempoCañon;
    }

    void Update()
    {
        DispararBala();
    }

    private void DispararBala()
    {
        tiempo -= Time.deltaTime;
        if (tiempo < 0)
        {
            GameObject nuevaBala = Instantiate(bala, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity, this.transform);
            balas.Enqueue(nuevaBala);
            switch (direccionSeleccionada)
            {
                case Direccion.right:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(velocidad, 0, 0);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.right * Time.deltaTime);
                    break;
                case Direccion.left:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(velocidad * -1, 0, 0);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.left * Time.deltaTime);
                    break;
                case Direccion.up:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionX;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(0, velocidad, 0);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.up * Time.deltaTime);
                    break;
                case Direccion.down:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionX;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(0, velocidad * -1, 0);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.down * Time.deltaTime);
                    break;
                case Direccion.forward:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, velocidad * -1);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.forward * Time.deltaTime);
                    break;
                case Direccion.back:
                    nuevaBala.GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
                    nuevaBala.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, velocidad);
                    nuevaBala.GetComponent<Transform>().Translate(Vector3.back * Time.deltaTime);
                    break;
                default:
                    break;
            }
            tiempo = tiempoCañon;
        }
        if (balas.Count > cantidadBalas)
        {
            Destroy(balas.Peek());
            balas.Dequeue();
        }
    }

    public void DestruirBalas()
    {
        while (balas.Count > 0)
        {
            Destroy(balas.Peek());
            balas.Dequeue();
        }
        tiempo = tiempoCañon;
    }
}
