using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    public float valorCronometro, copiaValorOrigninal;
    public Canvas canvas;
    public GameObject Jugador, Checkpoints, Cañones, Saltarines, PowerUps, Persecutores, Aplastadores, Monedas;
    [HideInInspector] public List<GameObject> checkpoints, powerUps;
    [HideInInspector] public int cpActual, cantidadMonedas;

    private float tiempoRestante, tiempo;
    private bool gano, perdio;
    private ControlJugador cj;
    private int monedas;

    void Start()
    {
        StartCoroutine(ComenzarCronometro(valorCronometro));
        copiaValorOrigninal = valorCronometro;
        tiempo = 2;
        monedas = 0;
        foreach (Transform monedasTransform in Monedas.transform)
        {
            cantidadMonedas++;
        }
        Transform textMonedas = canvas.transform.Find("TextoMonedas");
        textMonedas.GetComponent<TextMeshProUGUI>().text = ("Monedas: " + monedas + "/" + cantidadMonedas);
        foreach (Transform CheckpointTransform in Checkpoints.transform)
        {
            GameObject Chekpoint = CheckpointTransform.gameObject;
            checkpoints.Add(Chekpoint);
        }
        foreach (Transform powerUpsTransform in PowerUps.transform)
        {
            GameObject PowerUps = powerUpsTransform.gameObject;
            powerUps.Add(PowerUps.GetComponent<Transform>().gameObject);
        }
        for (int i = 0; i < checkpoints.Count; i++)
        {
            checkpoints[i].GetComponent<ControlChekpoint>().cp = i;
        }
        cpActual = 0;
        cj = Jugador.GetComponent<ControlJugador>();
        cj.transform.position = new Vector3(checkpoints[cpActual].transform.position.x + checkpoints[cpActual].transform.localScale.x / 2, checkpoints[cpActual].transform.position.y + 2, checkpoints[cpActual].transform.position.z);

    }

    void Update()
    {
        if (perdio || gano)
        {
            Time.timeScale = 0f;
            tiempo -= Time.unscaledDeltaTime;
            if (perdio)
            {
                canvas.transform.Find("TextoPerdiste").gameObject.SetActive(true);
            }
            else if (gano)
            {
                checkpoints[cpActual].GetComponent<ControlChekpoint>().CambiarMaterial(2);
                canvas.transform.Find("TextoGanaste").gameObject.SetActive(true);
            }
            if (tiempo < 0)
            {
                canvas.transform.Find("TextoGanaste").gameObject.SetActive(false);
                canvas.transform.Find("TextoPerdiste").gameObject.SetActive(false);
                gano = false;
                perdio = false;
                tiempo = 2;
                valorCronometro = copiaValorOrigninal;
                Time.timeScale = 1f;
                Reiniciar(true);
            }
        }
        else
        {
            Reiniciar(false);
        }
    }

    public void Reiniciar(bool recibioDaño)
    {
        if (Input.GetKeyDown(KeyCode.R) || perdio || gano)
        {
            ReiniciarTimer(copiaValorOrigninal);
            checkpoints[cpActual].GetComponent<ControlChekpoint>().CambiarMaterial(1);
            cpActual = 0;
            checkpoints[cpActual].GetComponent<ControlChekpoint>().CambiarMaterial(2);
            cj.Reiniciar(checkpoints, cpActual);
            ReiniciarMonedas(true);
            for (int powerUp = 0; powerUp < powerUps.Count; powerUp++)
            {
                powerUps[powerUp].GetComponent<ControlPowerUp>().Reiniciar();
            }
            foreach (Transform PersecutorTransform in Persecutores.transform)
            {
                PersecutorTransform.gameObject.GetComponent<ControlPersecutor>().Reiniciar();
            }
            foreach (Transform aplastadorTransform in Aplastadores.transform)
            {
                aplastadorTransform.gameObject.GetComponent<ControlAplastador>().Reiniciar();
            }
        }
        else if (cj.DebajoDelSuelo() || recibioDaño)
        {
            ReiniciarTimer(valorCronometro);
            ReiniciarMonedas(false);
            foreach (Transform CañonTransform in Cañones.transform)
            {
                CañonTransform.gameObject.GetComponent<ControlCañon>().DestruirBalas();
            }
            for (int powerUp = 0; powerUp < powerUps.Count; powerUp++)
            {
                powerUps[powerUp].GetComponent<ControlPowerUp>().Reiniciar();
            }
            cj.Reiniciar(checkpoints, cpActual);
            foreach (Transform PersecutorTransform in Persecutores.transform)
            {
                PersecutorTransform.gameObject.GetComponent<ControlPersecutor>().Reiniciar();
            }
            foreach (Transform aplastadorTransform in Aplastadores.transform)
            {
                aplastadorTransform.gameObject.GetComponent<ControlAplastador>().Reiniciar();
            }
        }

    }

    public IEnumerator ComenzarCronometro(float valorCronometro)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Transform textTiempoRestante = canvas.transform.Find("TextoTiempoRestante");
            textTiempoRestante.GetComponent<TextMeshProUGUI>().text = ("Tiempo Restante: " + (tiempoRestante - 1));
            Transform textMonedas = canvas.transform.Find("TextoMonedas");
            textMonedas.GetComponent<TextMeshProUGUI>().text = ("Monedas: " + monedas + "/" + cantidadMonedas);

            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
            if (tiempoRestante == 0)
            {
                canvas.transform.Find("TextoPerdiste").gameObject.SetActive(true);
                perdio = true;
            }
        }
    }

    private void ReiniciarTimer(float copiaValorOrigninal)
    {
        valorCronometro = copiaValorOrigninal;
        tiempoRestante = valorCronometro;
        StopAllCoroutines();
        StartCoroutine(ComenzarCronometro(valorCronometro));
    }

    public void CambiarCheckpoint(bool cambiar, ControlChekpoint cj)
    {
        if (cambiar)
        {
            valorCronometro = tiempoRestante;
            checkpoints[cpActual].GetComponent<ControlChekpoint>().CambiarMaterial(1);
            cpActual = cj.cp;
            cj.CambiarMaterial(2);
            if (cpActual == checkpoints.Count - 1)
            {
                gano = true;
            }
        }
    }

    public void SumarMoneda(bool sumar, GameObject moneda)
    {
        if (sumar)
        {
            monedas++;
            moneda.GetComponent<ControlMoneda>().AgarrarMoneda();
            Transform textMonedas = canvas.transform.Find("TextoMonedas");
            textMonedas.GetComponent<TextMeshProUGUI>().text = ("Monedas: " + monedas + "/" + cantidadMonedas);
        }
    }

    public void ReiniciarMonedas(bool reiniciar)
    {
        monedas = 0;
        foreach (Transform MonedasTransform in Monedas.transform)
        {
            if (reiniciar)
            {
                monedas = 0;
                MonedasTransform.GetComponent<ControlMoneda>().agarrado = false;
                MonedasTransform.gameObject.GetComponent<CapsuleCollider>().enabled = true;
                MonedasTransform.GetComponent<MeshRenderer>().enabled = true;
            }
            if (MonedasTransform.GetComponent<ControlMoneda>().agarrado)
            {
                monedas++;
                MonedasTransform.gameObject.GetComponent<CapsuleCollider>().enabled = false;
                MonedasTransform.GetComponent<MeshRenderer>().enabled = false;
            }
        }
        
        Transform textMonedas = canvas.transform.Find("TextoMonedas");
        textMonedas.GetComponent<TextMeshProUGUI>().text = ("Monedas: " + monedas + "/" + cantidadMonedas);
    }
}
