using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMoneda : MonoBehaviour
{
    public bool agarrado;

    public void AgarrarMoneda()
    {
        agarrado = true;
        GetComponent<MeshRenderer>().enabled = false;
    }
}
