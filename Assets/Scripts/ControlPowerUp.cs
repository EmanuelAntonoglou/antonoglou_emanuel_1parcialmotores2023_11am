using System.Collections;
using System.Collections.Generic;
using TMPro;
using TreeEditor;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

public class ControlPowerUp : MonoBehaviour
{
    public float duracion, aumentoVelocidad;
    [HideInInspector] public GameObject Jugador;

    private float duracionOriginal;

    void Start()
    {
        Jugador = GameObject.Find("Jugador");
        duracionOriginal = duracion;
    }

    public void Reiniciar()
    {
        StopAllCoroutines();
        duracion = duracionOriginal;
        GetComponent<CapsuleCollider>().enabled = true;
        GetComponent<MeshRenderer>().enabled = true;
    }

    public void PowerUpOn()
    {
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        StartCoroutine(ComenzarCronometro());
        Jugador.transform.localScale -= new Vector3(0.75f, 0.75f, 0.75f);
        Jugador.GetComponent<MeshRenderer>().material = Jugador.GetComponent<ControlJugador>().matPowerUp;
        Jugador.GetComponent<ControlJugador>().velocidad += aumentoVelocidad;
    }

    public IEnumerator ComenzarCronometro()
    {
        while (duracion > 0)
        {
            yield return new WaitForSeconds(1.0f);
            duracion--;
            if (duracion == 0)
            {
                Jugador.transform.localScale += new Vector3(0.75f, 0.75f, 0.75f);
                Jugador.GetComponent<MeshRenderer>().material = Jugador.GetComponent<ControlJugador>().matOriginal;
                Jugador.GetComponent<ControlJugador>().velocidad -= aumentoVelocidad;
            }
        }
    }
}
