using System.Collections.Generic;
using UnityEngine;

public class ControlChekpoint : MonoBehaviour
{
    public Material matOriginal, matMarcado;
    
    [HideInInspector]  public int cp;
    
    public void CambiarMaterial(int numMat)
    {
        if (numMat == 1)
        {
            GetComponent<MeshRenderer>().material = matOriginal;
        }
        else if (numMat == 2)
        {
            GetComponent<MeshRenderer>().material = matMarcado;
        }
    }
}
