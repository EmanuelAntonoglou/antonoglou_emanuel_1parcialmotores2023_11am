using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPersecutor : MonoBehaviour
{
    public int rapidez;
    public GameObject checkpoint;
    [HideInInspector] public GameObject gameManager;

    private GameObject jugador;
    private Vector3 posOriginal;
    
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        jugador = GameObject.Find("Jugador");
        posOriginal = transform.position;
    }

    private void Update()
    {
        DebajoDelSuelo();
    }

    void FixedUpdate()
    {
        Moverse();
    }

    public void Reiniciar()
    {
        transform.position = posOriginal;
    }

    private void Moverse()
    {
        if (gameManager.GetComponent<GameManager>().cpActual == checkpoint.GetComponent<ControlChekpoint>().cp)
        {
            transform.LookAt(jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
    }
    public void DebajoDelSuelo()
    {
        if (transform.position.y < -5)
        {
            Reiniciar();
        }
    }
}
