using UnityEngine;

public class ControlSaltarin : MonoBehaviour
{
    public float delaySalto;
    public float magnitudSalto;
    public LayerMask layerPiso;
    [HideInInspector] public GameObject personaje;

    private Rigidbody rb;
    private BoxCollider col;
    public float tiempo1, tiempo2;
    public bool salto;
    private Vector3 posOriginal;
    public float saltoPj;

    void Start()
    {
        personaje = GameObject.Find("Jugador");
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        posOriginal = transform.position;
        tiempo1 = delaySalto;
        tiempo2 = delaySalto;
        salto = false;
    }

    void Update()
    {
        Saltar();
    }

    public void Saltar()
    {
        if (personaje.GetComponent<ControlJugador>().salto || personaje.GetComponent<ControlJugador>().dobleSalto)
        {
            if (personaje.GetComponent<ControlJugador>().transform.position.y > personaje.GetComponent<ControlJugador>().posOriginal.y)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(posOriginal.x, posOriginal.y + magnitudSalto, posOriginal.z), 5000);
            }
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.CheckBox(col.bounds.center, new Vector3(col.bounds.size.x * 0.45f, col.bounds.size.y * 0.45f, col.bounds.size.z * 0.45f), Quaternion.identity, layerPiso);
    }
}
