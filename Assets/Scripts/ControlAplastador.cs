using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAplastador : MonoBehaviour
{
    public float tiempo;
    public GameObject GameManager, Jugador;
    public LayerMask layerPiso;

    private Vector3 posOriginal;
    private BoxCollider col;
    private float tiempoActual;

    void Start()
    {
        Jugador = GameObject.Find("Jugador");
        GameManager = GameObject.Find("GameManager");
        col = GetComponent<BoxCollider>();
        posOriginal = transform.position;
        tiempoActual = 0f;
    }

    public void Reiniciar()
    {
        StopAllCoroutines();
        GetComponent<BoxCollider>().enabled = true;
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        transform.position = posOriginal;
        tiempoActual = 0f;
    }


    public void Caer()
    {
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<Rigidbody>().useGravity = true;
        GetComponent<Transform>().Translate(Vector3.down * Time.deltaTime);
    }

    IEnumerator Subir()
    {
        while (tiempoActual < tiempo)
        {
            tiempoActual += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, posOriginal, tiempoActual / tiempo);
            if (transform.position == posOriginal)
            {
                Reiniciar();
            }
            yield return null;
        }
    }

    public bool EstaEnPiso()
    {
        return Physics.CheckBox(col.bounds.center, new Vector3(col.bounds.extents.x, 0.1f, col.bounds.extents.z), col.transform.rotation, layerPiso);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            GetComponent<Rigidbody>().useGravity = false;
            StartCoroutine(Subir());
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.GetComponent<GameManager>().Reiniciar(true);
        }
    }
}
