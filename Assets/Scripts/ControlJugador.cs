using System.Collections;
using System.Collections.Generic;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.ProBuilder.Shapes;

public class ControlJugador : MonoBehaviour
{
    public float velocidad, magnitudSalto;
    public LayerMask layerPiso;
    public GameObject gameManager;
    public Material matOriginal, matPowerUp;
     public bool salto, dobleSalto;
    [HideInInspector] public float velocidadOriginal;

    private Rigidbody rb;
    private SphereCollider col;
    private GameManager gm;
    private float movimientoHorizontal, movimientoVertical;
    public int saltos;
    public Vector3 posOriginal;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        gm = gameManager.GetComponent<GameManager>();
        posOriginal = transform.position;
        saltos = 2;
        salto = false;
        dobleSalto = false;
        velocidadOriginal = velocidad;
    }

    private void Update()
    {
        Saltar();
    }

    private void FixedUpdate()
    {
        Mover();
    }

    public void Reiniciar(List<GameObject> checkpoints, int cpActual)
    {
        saltos = 2;
        salto = false;
        dobleSalto = false;
        transform.localScale = Vector3.one;
        GetComponent<MeshRenderer>().material = GetComponent<ControlJugador>().matOriginal;
        GetComponent<ControlJugador>().velocidad = GetComponent<ControlJugador>().velocidadOriginal;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        transform.position = new Vector3(checkpoints[cpActual].transform.position.x + checkpoints[cpActual].transform.localScale.x / 2, checkpoints[cpActual].transform.position.y + 2, checkpoints[cpActual].transform.position.z + checkpoints[cpActual].transform.localScale.z / 2);
    }

    private void Mover()
    {
        PedirInput();
        Vector3 fuerzaMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        fuerzaMovimiento.Normalize();
        rb.AddForce(fuerzaMovimiento * velocidad);
    }

    private void Saltar()
    {
        if (Input.GetKeyDown(KeyCode.Space) && saltos > 0)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            saltos--;
        }
        if (saltos == 2)
        {
            salto = false;
            dobleSalto = false;
        }
        if (saltos == 1)
        {
            salto = true;
            dobleSalto = false;
        }
        if (saltos == 0)
        {
            salto = false;
            dobleSalto = true;
        }
    }

    private void PedirInput()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");
    }

    public bool DetectarEspacio()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            return true;
        }
        return false;
    }

    public bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, layerPiso);
    }

    public bool DebajoDelSuelo()
    {
        if (transform.position.y < -5)
        {
            return true;
        }
        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Piso"))
        {
            saltos = 2;
        }
        if (other.gameObject.CompareTag("Moneda"))
        {
            gm.SumarMoneda(true, other.gameObject);
            other.gameObject.GetComponent<CapsuleCollider>().enabled = false;
        }
        gm.CambiarCheckpoint(other.gameObject.CompareTag("Checkpoint"), other.GetComponent<ControlChekpoint>());
        gm.Reiniciar(other.gameObject.CompareTag("Enemigo"));
        if (other.gameObject.CompareTag("PowerUp"))
        {
            other.gameObject.GetComponent<ControlPowerUp>().PowerUpOn();
        }
        if (other.gameObject.CompareTag("DebajoDeObjeto"))
        {
            other.gameObject.GetComponent<ControlAplastador>().Caer();
        }
    }
}
